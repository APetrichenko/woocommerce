<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'woocombd');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K-t4`p,*v1|.)_G5wS1qtsVS2;pRozA0r?%_E60!UT31a 2P{Qd*&c_7LGd,RNcn');
define('SECURE_AUTH_KEY',  ']{%:i5s&tg?0]:_af1_JIq?0dZP5Lt><)vu6Xt([F}/fpQ#:h: )X:EwYaV90TJc');
define('LOGGED_IN_KEY',    '{F|.C;WX[K(!`4J/eEmZ=leuO?q4k|[v;cwNF?8bT<8(y  [v)- BRO<pq5wg]~E');
define('NONCE_KEY',        'kh1dZjvJjXdjqt?$MMEJ.+{p&^SLikB&<AFZ EuM/l+H/N>,-h<)a x$WC@/mkcQ');
define('AUTH_SALT',        '7n`4e{,!M*luC1XSIh|Jf[$m$6M9?%ZCCsa.81L0U90L#i<9Fhs5b)-418^-=!$y');
define('SECURE_AUTH_SALT', '0k$K;|obO(D<tzSr,_SI){&BAN6X+hTBLzFnV|IhY.=>$v>w.LTqOz=mihnE&vGb');
define('LOGGED_IN_SALT',   'Dt>VoX3>r] aTM5,G&Ma *`@$1NRnQu3AaVL/g:|o:wGo7n9JM}pP,57ylJr|KR)');
define('NONCE_SALT',       'Oo@Aw!n/&Nl!jg+4y3B,9&%loiOe}5 FE,r:4In+b>Huz,C_&60iI1H,u*]uBRf8');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
