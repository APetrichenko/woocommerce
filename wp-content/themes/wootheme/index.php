<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */
if (wc_get_page_id( 'cart' ) == get_the_ID()) {
	echo do_shortcode('[woocommerce_cart]');
}
else if (wc_get_page_id( 'checkout' ) == get_the_ID()) {
		echo do_shortcode('[woocommerce_checkout]');
	}
else {

get_header(); ?>

<?php if ( have_posts() ) {
		while ( have_posts() ) { 
			the_post();
			//get_template_part( 'content', get_post_format() );
			?>
			<?php
			
			$products_popular = wc_get_products( array(
				'tag' => 'popular', ) );
			?>

			<div class="home-slider container js-home-slider">
				<?php foreach ($products_popular as $product_popular) { ?>
				          		<div class="item"><?php echo $product_popular->get_image('slide'); ?> </div>
				<?php } ?>
        	</div>
        <!-- end home-slider -->

        <h1 class="title"><span>популярные товары</span></h1>
        <!-- begin popular-wallpapers  -->
		<?php echo do_shortcode('[products limit = "6" columns = "3" visibility = "featured" class = "wallpapers container" ]'); ?>
        <a class="go-directory" href="">Перейти в каталог</a>
        <!-- end popular-wallpapers -->
        <!-- begin why  -->
		<?php
			$query = new WP_Query('category_name=why');
			if ( $query->have_posts() ) {		
		?>
				<div class="why">
					<div class="container">
						<div class="why__item">
							Почему наши товары лучшие
						</div>
						<?php while ( $query->have_posts() ) { 
							$query->the_post();
						?>
						<div class="why__item">
							<div class="why__col"><?php the_post_thumbnail(); ?></div>
							<div class="why__col">
								<div class="why__title"><?php the_title(); ?></div>
								<div class="why__content">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>

		<?php } ?>
	<?php
		get_template_part( 'loop' );
		}		
}
else {
	get_template_part( 'content', 'none' );
}
 ?>


<?php
	get_footer();
	}
?>
