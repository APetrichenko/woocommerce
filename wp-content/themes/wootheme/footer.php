<?php
/**
 * Auth footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/auth/footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Auth
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
		<div class="footer-info">
            <div class="container">
                <p>Не нашли, что искали? <br>
                    Может быть вы просто что-то пропустили. <br>
                    Посмотрите наш каталог еще раз. </p>
                <a class="footer-info__link" href="">Вернуться в каталог</a>
            </div>
        </div>
        <!-- begin footer  -->
<div class="footer">
    <div class="container">
        <div class="footer__item footer__menu">
            <ul>
                <li><a href="">Инструкция по оклейке</a></li>
                <li><a href="">Контакты</a></li>
            </ul>
        </div>
        <?php if( have_rows( 'socials', 'option' ) ):?>
        <div class="footer__item social-menu">
            <span class="social-menu__title">Поделиться</span>
            <ul>
                <?php while ( have_rows('socials', 'option') ) : the_row();
                    $image = get_sub_field('image');
                    ?>
                    <li><a href="<?php the_sub_field('link'); ?>" target="_blank"> <?php echo wp_get_attachment_image( $image, 'full'); ?></a></li>
                <?php endwhile; ?>
            </ul>
        </div>
        <?php endif; ?>
    </div>
</div>
<!-- end footer -->
        </div>
    <!-- end wrap -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="/wp-content/themes/wootheme/js/vendors.min.js"></script>
<script src="/wp-content/themes/wootheme/js/main.min.js"></script>
    <?php wp_footer(); ?>
	</body>
</html>
