<?php

if( function_exists( 'acf_add_options_page' ) ) {
	
	acf_add_options_page();
	
}

add_theme_support( 'menus' );
function my_theme_enqueue_styles() {
	
	wp_enqueue_style( 'vendors', get_template_directory_uri() . '/css/vendors.min.css' );
	//wp_enqueue_script('cdnjs', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js');
	wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700');
	wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.min.css' );
	//wp_enqueue_script('vendorsjs', get_template_directory_uri() . '/js/vendors.min.js');
	//wp_enqueue_script('mainjs', get_template_directory_uri() . '/js/main.min.js');
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

add_image_size( 'custom_thumbnail', 360, 360);
add_image_size( 'slide', 1200, 600);
//remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
//remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
//remove_action( 'woocommerce_before_main_content', 'WC_Structured_Data::generate_website_data()', 30);


//add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);

function my_theme_wrapper_start() {
	
echo '<div class="wrap">
		<div class="content container">';

}

//add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_end() {
	echo '</div>';
}

//remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
//remove_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );

//add_action( 'woocommerce_archive_description', 'title_categoty', 10 );

function title_categoty() {
	echo '<div class="content ">
				<h1 class="title"><span>Товары в категории: '; single_term_title(); echo '</span></h1>';
}

//add_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
//remove_action( 'woocommerce_shop_loop', 'WC_Structured_Data::generate_product_data()', 10 );
//add_action( 'woocommerce_shop_loop', 'show_product', 10 );

function show_product() {
	echo '
	<div class="wallpapers__category">
							<div class="wallpapers__item action">';
							   the_post_thumbnail('custom_thumbnail');
							   echo '
							   <div class="wallpapers__info">
								   <div class="wallpapers__info-item wallpapers__title">';the_title(); echo '</div>
								   <div class="wallpapers__info-item">
									   <a class="wallpapers__link" href="">Посмотреть</a>
								   </div>
							   </div>
							</div>
						</div>';
}

//remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
//remove_action( 'woocommerce_no_products_found', 'wc_no_products_found', 10 );
//remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
  
function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_address_1']);
	unset($fields['billing']['billing_city']);
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_state']);
	unset($fields['billing']['order_comments']);
	unset($fields['account']['account_username']);
	unset($fields['account']['account_password']);
	unset($fields['account']['account_password-2']);
	unset($fields['shipping']['shipping_first_name']);
	unset($fields['shipping']['shipping_last_name']);
	unset($fields['shipping']['shipping_company']);
	unset($fields['shipping']['shipping_address_2']);
	unset($fields['shipping']['shipping_postcode']);
	unset($fields['shipping']['shipping_country']);
	$fields['billing']['billing_first_name']['label'] = "Имя";
	$fields['billing']['billing_last_name']['label'] = "Фамилия";
	$fields['billing']['billing_phone']['label'] = "Телефон";
	$fields['billing']['billing_email']['label'] = "Электронная почта";
	$fields['shipping']['home']['placeholder'] = "Дом";
	$fields['shipping']['home']['id'] = "home";
	$fields['shipping']['apartment']['placeholder'] = "Квартира";
	$fields['shipping']['apartment']['id'] = "apartment";
	$fields['shipping']['shipping_address_1']['placeholder'] = "Улица";
	$fields['shipping']['shipping_address_1']['id'] = "shipping_address_1";
	$fields['shipping']['shipping_address_1']['label'] = "";
	$fields['shipping']['shipping_address_1']['required'] = false;
	$fields['shipping']['shipping_city']['required'] = false;
	$fields['shipping']['shipping_city']['label'] = "Город";
	$fields['shipping']['shipping_city']['type'] = "select";
	$fields['shipping']['shipping_city']['class'] = "custom-select js-custom-select";
	$fields['shipping']['shipping_city']['options'] = array(
		'select' => 'Выбрать',
		'omsk' => 'Омск',
		'moskva' => 'Москва',
		'ishim' => 'Ишим',
		'piter' => 'Питер',

	 );
	$fields['shipping']['shipping_state']['required'] = false;
	$fields['shipping']['shipping_state']['label'] = "Область";
	$fields['shipping']['shipping_state']['type'] = "select";
	$fields['shipping']['shipping_state']['input_class'] = array("custom-select js-custom-select");
	$fields['shipping']['shipping_state']['class'] = null;	
	$fields['shipping']['shipping_state']['options'] = array(
		'select' => 'Выбрать',
		'omskaya' => 'Омск',
		'moskovskaya' => 'Московкая',
		'tumenskaya' => 'Тюменская',
		'piterskaya' => 'Питерская',
	 );
	return $fields;
}

add_filter ( 'woocommerce_page_title', 'woo_shop_page_title' );
function woo_shop_page_title ( $page_title ) {
	if ( 'Shop' == $page_title ) {
		return "Shop" ;
	}
}
?>