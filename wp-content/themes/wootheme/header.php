<?php
/**
 * Auth header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/auth/header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Auth
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php if ( is_shop() ) { ?>
        <title>Shop</title>
    <?php } else ?>
        <title><?php the_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body>
	<!-- begin header-top  -->
<div class="header-top js-header-top">
    <div class="container">
        <div class="header-top__item">
            <a class="header-top__city header__link" href="">Омск</a>
            <a class="header-top__address header__link" href="">ул. Гагарина, 8/2</a>
            <a class="header-top__cart header__link" href="">Корзина (3)</a>
            <div class="header-top-menu">
                <ul>
                    <li class="current-menu-item"><a>АКЦИИ</a></li>
                    <li><a href="">ФОТООБОИ</a></li>
                    <li><a href="">ОПЛАТА и ДОСТАВКА</a></li>
                </ul>
            </div>
        </div>
        <div class="header-top__item">
            <button class="header-top__phone-call" type="button">Заказать обратный звонок</button>
            <a class="header-top__number-phone" href="tel:+89087997555">8 <span>908</span> 799-75-55</a>
        </div>
    </div>
</div>
<!-- end header-top -->

<!-- begin header  -->
<div class="header container">
    <div class="header__item header__logo">
        <a href="" class="header__logo-link">
            <?php
            $image = get_field('logo', 'option');
            echo wp_get_attachment_image( $image, 'full'); ?>
        </a>
    </div>
    <div class="header__item header__menu">
        <ul>
            <li class="header__menu_stock"><a href="">Акции</a></li>
            <li class="current-menu-item header__menu_wallpapers"><a href="">Фотообои</a></li>
            <li class="header__menu_payment-shipping"><a href="">Оплата и доставка</a></li>
        </ul>
    </div>
</div>
<!-- end header -->