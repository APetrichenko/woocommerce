<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
?>

<div class="wrap">


        <!-- begin container  -->
        <div class="content container">

            <!-- begin breadcrumbs  -->
            <?php woocommerce_breadcrumb(); ?>
            <!-- end breadcrumbs -->
            <!-- begin sidebar -->
            <?php 
                $args = array('widget_id' => 'woocommerce_product_categories', 'before_widget' => '<div id="product_categories-1" class="sidebar sidebar-menu">', 'after_widget' => '</div><!-- //sidebar-widget -->', 'before_title' => '<h4 class="sidebar-menu__title">', 'after_title' => '</h4>');
                $instance = array('title' => 'Categories', 'hierarchical' => true, 'count' => false, 'dropdown' => false, 'orderby' => '');
                $widget = new WC_Widget_Product_Categories();
                $widget->widget($args, $instance); 
            ?>
            <!-- end sidebar -->
		<!-- begin product  -->
            <div class="product content">
                <div class="product__image">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="product__title"><?php the_title(); ?></div>
                <div class="product__container group">
                    <button class="btn btn_blue product__btn-like" type="button">Нравится этот товар</button>
                    <a class="btn product__btn-manual" href="">Инструкция по оклейке</a>
                    <?php if( have_rows( 'socials', 'option' ) ):?>
                    <div class="social-menu social-menu_product">
                        <span class="social-menu__title">Поделиться</span>
                        <ul>
                            <?php while ( have_rows('socials', 'option') ) : the_row();
                                $image = get_sub_field('image');
                              ?>
                                <li><a href="<?php the_sub_field('link'); ?>" target="_blank"> <?php echo wp_get_attachment_image( $image, 'full'); ?></a></li>
                                <?php endwhile; ?>
                        </ul>
                    </div>
                            <?php endif; ?>
                </div>
                <div class="product-items">
                    <div class="product__item action">                        
                        <div class="product__article">Артикул: <?php echo $product->get_sku(); ?></div>
                        <div class="product__size">Размер: <?php echo $product->get_attribute('size'); ?> </div>
                        <div class="product__number-bands">Цвет: <?php echo $product->get_attribute('color'); ?></div>
                        <div class="product__price"><?php echo $product->get_price_html(); ?></div>
                        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
                        <!-- <a class="btn_blue product__btn-buy" href="">Купить</a> -->
                    </div>                    
                </div>
            </div>
            <!-- end product -->
        </div>
        <!-- end container -->


            <?php
        $query = new WP_Query('category_name=why');
        if ( $query->have_posts() ) {
        
        ?>
        <div class="why">
            <div class="container">
                <div class="why__item">
                    Почему наши товары лучшие
                </div>
                <?php while ( $query->have_posts() ) { 
                	$query->the_post();
                	?>
                <div class="why__item">
                    <div class="why__col"><?php the_post_thumbnail(); ?></div>
                    <div class="why__col">
                        <div class="why__title"><?php the_title(); ?></div>
                        <div class="why__content">
                            <?php the_content(); ?>
                            </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <?php } ?>
