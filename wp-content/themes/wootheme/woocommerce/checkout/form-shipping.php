<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="cart__footer-col cart__shipping">
		<div class="cart__title cart__title_shipping">ДОСТАВКА</div>
            <div class="cart__shipping-row">
                <div class="cart__shipping-col">Доставка</div>
				<div class="cart__shipping-col _right">399 руб.</div>
            </div>
				<?php
					$fields = $checkout->get_checkout_fields( 'shipping' );
					?>
					<select class="custom-select js-custom-select" data-placeholder="Область" name="" id="">
                        <option></option>
                        <option>Омск</option>
                        <option>Омск</option>
                        <option>Омск</option>
                    </select>
                    <select class="custom-select js-custom-select" data-placeholder="Город" name="" id="">
                        <option></option>
                        <option>Омск</option>
                        <option>Омск</option>
                        <option>Омск</option>
                    </select>
                    <?php 
						//$shipping_methods = WC()->shipping->load_shipping_methods();
				
						//print_r($shipping_methods);
				 ?>
				 	<?php wc_cart_totals_shipping_html(); ?>
                    
                </div>
                <?php $order_comments = $checkout->get_checkout_fields( 'order' ) ?>
                <div class="cart__footer-col cart__information">
                    <div class="cart__title">Информация для доставки</div>
                    <p>Нажимая кнопку «Создать и оплатить», я подтверждаю
                        свою дееспособность, согласие на получение информации
                        от интернет-магазина и уведомлений о состоянии моих заказов. </p>
                        <p>Ниже вы можете указать любой комментарий к вашему заказу:</p>
                        <textarea name=<?php echo key($order_comments); ?> id=<?php echo key($order_comments); ?> cols="30" rows="10"></textarea>
                        <div style="display: none;" id="order_review" class="woocommerce-checkout-review-order">
                            <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                        </div>
                        <!-- <button class="btn btn_blue btn-create-payment">Создать и оплатить</button> -->
                        <?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="btn btn_blue btn-create-payment" name="woocommerce_checkout_place_order" id="place_order" value="Создать и оплатить"' . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>
                        <?php do_action( 'woocommerce_review_order_after_submit' ); ?>
                        <?php wp_nonce_field( 'woocommerce-process_checkout' ); ?>
                </div>	
