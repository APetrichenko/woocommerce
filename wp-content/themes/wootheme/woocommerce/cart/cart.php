<?php
	/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
get_header();
/** @global WC_Checkout $checkout */
wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>
<div class="wrap">	
        <!-- begin container -->
    <div class="content container">
        <!-- begin breadcrumbs -->
        <?php woocommerce_breadcrumb(); ?>
        <!-- end breadcrumbs -->

        <h1 class="title"><span>Корзина</span></h1>

        <div class="cart">
            <div class="wrap-cart__items">
            <?php 
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
	?>
                <div class="cart__item">
                    <div class="cart__item-col cart__wallpapers">
                        <?php
					$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

					if ( ! $product_permalink ) {
						echo $thumbnail;
					} else {
						printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
					}
				?>
                    </div>
                    <div class="cart__item-col cart__names">
                        <div class="cart__name">
                            <?php
					if ( ! $product_permalink ) {
						echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
					} else {
						echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
					}
					?>
                        </div>
                        <div class="cart__article">
                            Артикул: <?php echo $_product->get_sku() ?>
                        </div>
                    </div>
                    <div class="cart__item-col cart__quantity quantity">
                        <div class="wrap-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
                           
                            <?php
					if ( $_product->is_sold_individually() ) {
						$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
					} else {
						$product_quantity = woocommerce_quantity_input( array(
							'input_name'  => "cart[{$cart_item_key}][qty]",
							'input_value' => $cart_item['quantity'],
							'max_value'   => $_product->get_max_purchase_quantity(),
							'min_value'   => '0',
						), $_product, false );
					}

					echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
				?>
                        </div>
                    </div>
                    <div class="cart__item-col cart__price">
                        <div class="cart-price"><?php
					echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
				?>		</div>
                        
                    </div>
                    <div class="cart__item-col cart__delete">
                    
                    <?php
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a class="cart__delete-btn btn_blue" type="button" href="%s" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span>delete product</span></a>',
						esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $product_id ),
						esc_attr( $_product->get_sku() )
					), $cart_item_key );
				?>
                    </div>
                </div>
		<?php
	}
}
?>
                
            </div>

            <div class="cart-footer">
                <div class="cart-footer__item cart-footer__item_btn">
                    <a href="" class="btn btn__cart-footer">Продолжить покупки</a>
                    <input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>" />

                    <?php do_action( 'woocommerce_cart_actions' ); ?>
                    <?php wp_nonce_field( 'woocommerce-cart' ); ?>
                </div>
                

                <div class="cart-footer__item cart-footer__item_shipping">
                    <div class="cart-footer__item-title">ДОСТАВКА:</div>
                    <div class="cart-footer__item-price">399 руб.</div>
                </div>
                <div class="cart-footer__item cart-footer__item_total">
                    <div class="cart-footer__item-title">ИТОГО:</div>
                    <div class="cart-footer__item-price"><?php echo WC()->cart->get_cart_subtotal(); ?></div>
                </div>
            </div>
            <?php
		echo do_shortcode('[woocommerce_checkout]');
		/*$fields = $checkout->get_checkout_fields( 'billing' );
		foreach ( $fields as $key => $field ) {
			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
		}*/
		?>

            </div>

        </div>
<?php get_footer(); ?>
