<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/** @global WC_Checkout $checkout */
?>
		<?php if ( 1 < count( $available_methods ) ) : ?>
			<?php foreach ( $available_methods as $method ) : ?>
					<?php
						switch( $method->method_id ) {
							case 'flat_rate':
							?>
								<div class="cart__shipping-row _shipping">
									<div class="cart__shipping-col">
										<?php
										printf( '<input type="radio" name="shipping" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" %4$s />
												<label for="shipping_method_%1$d_%2$s">%5$s</label>',
												$index, sanitize_title( $method->id ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ), wc_cart_totals_shipping_method_label( $method ) );
												?>

									</div>
									<div class="cart__shipping-col _right"></div>
								</div>
								<?php $fields = WC()->checkout->get_checkout_fields( 'shipping' ); ?>
								<input type="text" placeholder=<?php echo $fields['shipping_address_1']['placeholder'] ?> id = <?php echo $fields['shipping_address_1']['id'] ?> name = <?php echo $fields['shipping_address_1']['id'] ?>>
								<div class="cart__shipping-row _home">
									<div class="cart__shipping-col">
										 <input type="text" id = <?php echo $fields['home']['id'] ?> placeholder=<?php echo $fields['home']['placeholder'] ?> name = <?php echo $fields['home']['id'] ?> >
									</div>
									<div class="cart__shipping-col">
										<input type="text" id = <?php echo $fields['apartment']['id'] ?> placeholder=<?php echo $fields['apartment']['placeholder'] ?> name = <?php echo $fields['apartment']['id'] ?> >
									</div>
								</div>
								<p>Срок доставки 5-7 дней <br> Курьер позвонит Вам на указанный номер телефона</p>
									<?php
								break;
						?>
						<?php case 'local_pickup': ?>
								<div class="cart__shipping-ro">
									<div class="cart__shipping-col">
										<?php
											printf( '<input type="radio" name="shipping" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" %4$s />
												<label for="shipping_method_%1$d_%2$s">%5$s</label>',
												$index, sanitize_title( $method->id ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ), wc_cart_totals_shipping_method_label( $method ) );
											
										?>
									</div>
									<div class="cart__shipping-col"></div>
								</div>
							<?php break; ?>
						<?php } ?>
			<?php endforeach; ?>
		<?php elseif ( 1 === count( $available_methods ) ) :  ?>
			<?php
				$method = current( $available_methods );
				printf( '%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />', $index, esc_attr( $method->id ), wc_cart_totals_shipping_method_label( $method ) );
				do_action( 'woocommerce_after_shipping_rate', $method, $index );
			?>
		<?php elseif ( WC()->customer->has_calculated_shipping() ) : ?>
			<?php echo apply_filters( is_cart() ? 'woocommerce_cart_no_shipping_available_html' : 'woocommerce_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) ); ?>
		<?php elseif ( ! is_cart() ) : ?>
			<?php echo wpautop( __( 'Enter your full address to see shipping costs.', 'woocommerce' ) ); ?>
		<?php endif; ?>

		<?php if ( $show_package_details ) : ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
		<?php endif; ?>

		<?php if ( ! empty( $show_shipping_calculator ) ) : ?>
			<?php woocommerce_shipping_calculator(); ?>
		<?php endif; ?>
	</td>
</tr>
