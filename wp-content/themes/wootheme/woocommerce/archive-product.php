<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

	<div class="wrap">
		<!-- begin container  -->
		<div class="content container">

			<!-- begin breadcrumbs  -->
			<?php woocommerce_breadcrumb(); ?>
			<!-- end breadcrumbs -->
			<!-- begin sidebar -->
			<?php 
				$args = array('widget_id' => 'woocommerce_product_categories', 'before_widget' => '<div id="product_categories-1" class="sidebar sidebar-menu">', 'after_widget' => '</div><!-- //sidebar-widget -->', 'before_title' => '<h4 class="sidebar-menu__title">', 'after_title' => '</h4>');
				$instance = array('title' => 'Categories', 'hierarchical' => true, 'count' => false, 'dropdown' => false, 'orderby' => '');
				$widget = new WC_Widget_Product_Categories();
				$widget->widget($args, $instance); 
			?>
			<!-- end sidebar -->
			<!-- begin category strstr( $_SERVER['REQUEST_URI'], 'shop')  -->
			<div class="content ">

				<?php if ( is_shop() ) { ?>
					<h1 class="title"><span>Категории товаров</span></h1>
					<div class="wallpapers__category">
						<?php woocommerce_product_subcategories(); ?>
					</div>
				<?php } else { ?>
							<h1 class="title"><span>Товары в категории: <?php single_term_title(); ?></span></h1>
							<?php if ( have_posts() ) { ?>
								<div class="wallpapers__category">
								<?php 
									while ( have_posts()) {
										the_post();
								?>
										<?php wc_get_template_part( 'content', 'product' ); ?>
									
								<?php } ?>
							</div>
							<?php	} ?>
				<?php } ?>
			</div>
			<!-- end category -->

		</div>
		<!-- end container -->


		<!-- begin why  -->
		<?php
		$query = new WP_Query('category_name=why');
		if ( $query->have_posts() ) {
		
		?>
		<div class="why">
			<div class="container">
				<div class="why__item">
					Почему наши товары лучшие
				</div>
				<?php while ( $query->have_posts() ) { 
					$query->the_post();
					?>
				<div class="why__item">
					<div class="why__col"><?php the_post_thumbnail(); ?></div>
					<div class="why__col">
						<div class="why__title"><?php the_title(); ?></div>
						<div class="why__content">
							<?php the_content(); ?>
							</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

		<?php } ?>
	

<?php get_footer(); ?>
